# Bet Tracker

A Vue.JS app that lets you track bets (ranked or standard) on your browser.

## Development

### Setup

1. Clone the repository.
1. Build the container: `docker-compose build`.
1. Run the container: `docker-compose up`.
1. Access via http://localhost:8080.

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
docker-compose exec app npm run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
docker-compose exec app npm run lint
```

### Format with [Prettier](https://prettier.io/)

```sh
docker-compose exec app npm run format
```

## Deployment

This app is currently deployed on Vercel. Changes in the `main` branch gets deployed automatically.

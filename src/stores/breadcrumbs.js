import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useBreadcrumbsStore = defineStore('breadcrumbs', () => {
  const defaultBreadcrumbs = [{ title: 'Home', link: '/' }]
  const breadcrumbs = ref(defaultBreadcrumbs)

  const $reset = function () {
    breadcrumbs.value = []
    breadcrumbs.value.push(defaultBreadcrumbs[0])
  }

  const addBreadcrumb = function (title, link) {
    breadcrumbs.value.push({ title: title, link: link })
  }

  return { breadcrumbs, $reset, addBreadcrumb }
})

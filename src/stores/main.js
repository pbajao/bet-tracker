import { defineStore } from 'pinia'
import { useStorage } from '@vueuse/core'
import { ref, computed } from 'vue'

export const useStore = defineStore('main', () => {
  const darkMode = useStorage(
    'darkMode/v1',
    window.matchMedia('(prefers-color-scheme: dark)').matches
  )

  const games = useStorage('games/v1', {})

  const isDarkMode = computed(() => {
    return darkMode.value
  })

  const toggleDarkMode = function () {
    darkMode.value = !darkMode.value
  }

  const addGame = function (details) {
    const uuid = crypto.randomUUID()

    const game = {
      id: uuid,
      name: details.name,
      players: details.players,
      createdAt: new Date(),
      rounds: {}
    }

    games.value[uuid] = game
  }

  const deleteGame = function (id) {
    delete games.value[id]
  }

  const generateRoundData = function (playersLosses) {
    let roundData = {}

    const totalGain = Object.values(playersLosses).reduce((accumulator, currentValue) => {
      return accumulator + parseInt(currentValue, 10)
    }, 0)

    for (const [player, loss] of Object.entries(playersLosses)) {
      let gain = -loss

      if (loss == 0) {
        gain = totalGain
      }

      roundData[player] = { loss: parseInt(loss, 10), gain: gain }
    }

    return roundData
  }

  const addRound = function (gameId, playersLosses) {
    const game = games.value[gameId]
    const uuid = crypto.randomUUID()

    if (game.rounds === undefined) {
      game.rounds = {}
    }

    game.rounds[uuid] = generateRoundData(playersLosses)
  }

  const updateRound = function (gameId, roundId, playersLosses) {
    const game = games.value[gameId]

    game.rounds[roundId] = generateRoundData(playersLosses)
  }

  const deleteRound = function (gameId, roundId) {
    const game = games.value[gameId]

    delete game.rounds[roundId]
  }

  return {
    darkMode,
    isDarkMode,
    toggleDarkMode,
    games,
    addGame,
    deleteGame,
    addRound,
    updateRound,
    deleteRound
  }
})

export const gamePath = function (game) {
  return '/games/' + game.id
}

export const gameTitle = function (game) {
  const creationTimestamp = new Date(game.createdAt).toLocaleDateString('en-US', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    hour12: true
  })

  return game.name + ' - ' + creationTimestamp
}

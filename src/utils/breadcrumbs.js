import { useBreadcrumbsStore } from '@/stores/breadcrumbs'
import { useRoute } from 'vue-router'

const store = useBreadcrumbsStore()

export const resetBreadcrumbs = function () {
  store.$reset()
}

export const addBreadcrumb = function (title) {
  const route = useRoute()

  store.addBreadcrumb(title, route.path)
}

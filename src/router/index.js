import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/HomeView.vue')
    },
    {
      path: '/games/new',
      name: 'add-game',
      component: () => import('../views/AddGame.vue')
    },
    {
      path: '/games/:id',
      name: 'view-game',
      component: () => import('../views/ViewGame.vue')
    }
  ]
})

export default router
